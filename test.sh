#!/usr/bin/env bash

feature="./features/web"
if [ "$#" = "1" ]; then
  feature="$feature/$1.feature"
fi

MATRAK_TEST_FEATURE=$feature docker-compose -f docker-compose.yml -f docker-compose.tests.yml up --exit-code-from tests
